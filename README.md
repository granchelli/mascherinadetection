# Mascherina Detection

## 1. Creo il Dataset

Ho utilizzato 100 immagini di questo dataset:
[Face Mask Dataset](https://www.kaggle.com/andrewmvd/face-mask-detection)

## 2. Divido le immagini in train e test

Creo due cartelle train e test dove vado a spostare le immagini scelte, in particolare un 80%/90% nel train ed il restante nel test

## 3. Utilizzo LabelImg per l'annotazione delle immagini

[LabelImg](https://github.com/tzutalin/labelImg) - LabelImg is a graphical image annotation tool.

Per ogni immagine viene generato un file **.xml**

Infine avrò una due cartelle del tipo:
```
train
|- img1.jpg
|- img1.xml
|- img2.jpg
|- img2.xml
|- ...
test
|- img90.jpg
|- img90.xml
|- img91.jpg
|- img91.xml
|- ...
```

## 4. Files xml to csv

Utilizzo lo script **xml_to_csv.py** ed ottengo due file **train_labels.csv** e **test_labels.csv**

## 5. Genero i files .record per l'addestramento

Modifico lo script **generate_tfrecord.py** con le classi da trovare, in questo caso: 
```
def class_text_to_int(row_label):
    if row_label == 'with_mask':
        return 1
    elif row_label == 'without_mask':
        return 2
    elif row_label == 'mask_weared_incorrect':
        return 3
    else:
        return None
```

Utilizzo lo script **generate_tfrecord.py** una volta per il train e una per il test
```
python generate_tfrecord.py --csv_input=**train_labels.csv path** --image_dir=**train images path** --output_path=train.record
python generate_tfrecord.py --csv_input=**test_labels.csv path** --image_dir=**test images path** --output_path=test.record
```
## 5. Genero il file label_map.pbtxt

Seguendo lo stesso ordine dello script **generate_tfrecord.py** creo un file **label_map.pbtxt** contenente le classi da trovare, in questo caso:
```
item {
    id: 1
    name: 'with_mask'
}
item {
    id: 2
    name: 'without_mask'
}
item {
    id: 3
    name: 'mask_weared_incorrect'
}
```

## 6. Per l'addestramento uso Google Colab

[Google Colab](http://colab.research.google.com)

Uso il file [*/mascherinadetection/ColabFiles/MascherinaDetectionModel.ipynb*](/ColabFiles/) e carico nel runtime i tre file necessari per l'addestramento:

* **label_map.pbtxt**
* **test.record**
* **train.record**

Nel file ho le seguenti sezioni:

* **Clono tensorflow models**
* **Installo le API di tensorflow object detection**
	* **Testo l'installazione delle API**
* **Importo i files di train** - in questa sezione vado a modificare i percorsi dei tre files caricati in precedenza
* **Scarico il modello pre-addestrato** - uso *efficientdet_d0_coco17_tpu-32*
	* **Copio il config file dalle API di tensorflow** - ci sono dei files di config pronti all'uso in */object_detection/configs/tf2/*
	* **Modifico il file di config**
* **Addestro il modello**
	* **Grafici TensorBoard**
* **Salvo il nuovo modello addestrato**

Cambio il tipo di runtime e lo imposto su GPU.

## 7. Test del modello addestrato

Test del modello con i file:

* [*/mascherinadetection/ColabFiles/MascherinaDetectionFoto.ipynb*](/ColabFiles/) - immagini da web (presenti in [*/mascherinadetection/test_images/*](/test_images/)) passate in input
* [*/mascherinadetection/ColabFiles/MascherinaDetectionWebCamFoto.ipynb*](/ColabFiles/) - immagine catturata da webcam
* *mascherina_detection_webcam.py* - da PC real-time mascherina detection (per test da PC python e le API di tensorflow installati)

## 8. Risultati

Presenti in [*/mascherinadetection/benchmark/*](/benchmark/)




